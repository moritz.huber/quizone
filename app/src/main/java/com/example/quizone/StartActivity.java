package com.example.quizone;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class StartActivity extends AppCompatActivity {

    //GUI Elements
    private Button startButton;
    private TextView tvName;
    private Spinner spQuestions, spCategories;

    //Dialog shows while categories are loading
    private ProgressDialog progressDialog;

    //main url for the api
    private String myUrl = "https://opentdb.com/api_category.php";

    //All inputs which are relevant for the Intent and therefore also for the MainActivity
    private String category, questionAmount, difficulty, userName;
    private int selectedDifficulty, categoryId;
    private RadioGroup rbDifficulty;

    //List to load Categories from Trivia-API
    private ArrayList<String> categoriesList = new ArrayList();

    //Necessary when backButton is pressed on device
    private long backPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        startButton = (Button) findViewById(R.id.startButton);
        tvName = (TextView) findViewById(R.id.tvName);
        spQuestions = (Spinner) findViewById(R.id.spQuestions);
        spCategories = (Spinner) findViewById(R.id.spCategories);
        rbDifficulty = (RadioGroup) findViewById(R.id.rbdifficulty);
        rbDifficulty.check(R.id.rbNormal);

        //getCategories from the API
        fillCategories();


        startButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //check if Name is not Empty and has more Chars than 3
                if(tvName.getText().toString().isEmpty()){
                    Toast.makeText(StartActivity.this, "Please enter an username!", Toast.LENGTH_SHORT).show();
                }else if(tvName.getText().toString().length()<=3){
                    Toast.makeText(StartActivity.this, "Username has to be longer than 3 Characters!", Toast.LENGTH_SHORT).show();
                }else{
                    String name = tvName.getText().toString();
                    category = spCategories.getSelectedItem().toString();
                    categoryId = spCategories.getSelectedItemPosition();
                    questionAmount = spQuestions.getSelectedItem().toString();
                    selectedDifficulty = rbDifficulty.getCheckedRadioButtonId();
                    RadioButton selectedRadioButton= (RadioButton) findViewById(selectedDifficulty);
                    difficulty = selectedRadioButton.getText().toString().toLowerCase();

                    //give Information to the next Activity
                    Intent intent = new Intent(StartActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("name", name);
                    bundle.putString("category", category);
                    bundle.putString("questionAmount", questionAmount);
                    bundle.putString("difficulty", difficulty);
                    bundle.putInt("categoryId", categoryId);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    private void fillCategories() {
        StartActivity.MyAsyncTasks myAsyncTasks = new StartActivity.MyAsyncTasks();
        myAsyncTasks.execute();
    }


    //New Thread for getting the Categories
    public class MyAsyncTasks extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StartActivity.this);
            progressDialog.setMessage("processing results");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... strings) {
            String categories ="";
            try{
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(myUrl);
                    urlConnection = (HttpURLConnection)  url.openConnection();
                    InputStream in = urlConnection.getInputStream();
                    InputStreamReader isr = new InputStreamReader(in);
                    int data = isr.read();
                    while (data != -1){
                        categories += (char) data;
                        data = isr.read();
                    }

                    return categories;
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if (urlConnection != null){
                        urlConnection.disconnect();
                    }
                }
            }catch(Exception ex){
                ex.printStackTrace();
                return "Exception: " + ex.getMessage();
            }

            return categories;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("trivia_categories");

                for(int i = 0; i<jsonArray.length(); i++){
                    JSONObject jsonObjectsArray = jsonArray.getJSONObject(i);
                    categoriesList.add(jsonObjectsArray.getString("name"));

                }

                //Add the Elements in the list
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(StartActivity.this, android.R.layout.simple_spinner_item, categoriesList);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spCategories.setAdapter(arrayAdapter);

            }catch(JSONException jsonException){
                jsonException.printStackTrace();
            }
        }
    }

    //only exits when back Button is pressed 2 times in 2 second
    @Override
    public void onBackPressed() {
        if(backPressedTime + 2000 > System.currentTimeMillis()){
            super.onBackPressed();
        }else{
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }
}