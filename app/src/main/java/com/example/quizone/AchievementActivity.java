package com.example.quizone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AchievementActivity extends AppCompatActivity {

    //GUI Elements
    private Button newGameButton;
    private TextView userNameAchievement, endPointsAchieved;

    //get from Intent (MainActivity) for Userinterface
    private String userName;
    private int points, questionAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievement);

        //GUI Elements
        newGameButton = (Button) findViewById(R.id.backButton);
        userNameAchievement = (TextView) findViewById(R.id.tvUserNameAchievement);
        endPointsAchieved = (TextView) findViewById(R.id.tvEndPointsAchievement);

        //Intent from MainActivity (points and username)
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        userName = bundle.getString("name");
        points = bundle.getInt("points");
        questionAmount = bundle.getInt("questionAmount");

        //set Text in textView on screen
        userNameAchievement.setText(userName);
        endPointsAchieved.setText(points + "/" + questionAmount);

        //add listener to new Game button
        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNewgame();
            }
        });
    }

    //start New Game
    private void startNewgame() {
        //no data needs to be moved
        Intent startIntent = new Intent(AchievementActivity.this, StartActivity.class);
        startActivity(startIntent);
    }

    //when backbutton is pressed, a new game starts
    @Override
    public void onBackPressed() {
        startNewgame();
    }

}