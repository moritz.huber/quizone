package com.example.quizone;


import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    //30 seconds Timer
    private static final long TIMER_IN_MILLIES = 30000;

    //main Url for Get Requests
    private String myUrl = "https://opentdb.com/api.php?";

    //GUI Elements
    private TextView tvQuestionNumber, tvQuestion, tvPoints, tvUserName, tvTimer;
    private ProgressDialog progressDialog;
    private Button button1, button2, button3, button4, btnFullScreen;
    private ProgressBar progressBar;

    //necessary for get Intent from StartActivity
    private int questionAmount;
    private String category;
    private String difficulty;
    private int categoryId;

    //Arrays for storing all Answers and Questions
    //all 4 Answers per Question
    private String[][] allAnswers;
    //The one correct answer per Question
    private String[] answer;
    private String[] questionArray;

    //variables for Userface
    private int count = 0;
    private String userName;
    private int points = 0;

    //For Timer to change to red when under 10 seconds
    private ColorStateList textColorDefaultTimer;

    //Timer
    private CountDownTimer countDownTimer;
    private long timeLeftInMillies;

    //Colors of the buttons as finals Strings
    private final String btnDefaultColor = "#FF01579B";
    private final String btnCorrectColor = "#46d82f";
    private final String btnWrongColor = "#ef1515";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get Intent from StartActivity (which categroy, username, how many questions)
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        userName = bundle.getString("name");
        questionAmount = Integer.parseInt(bundle.getString("questionAmount"));
        category = bundle.getString("category");
        difficulty = bundle.getString("difficulty");
        categoryId = bundle.getInt("categoryId");

        //set the URL with elements from the Intent
        myUrl = myUrl.concat(setUrl());

        //GUI Elements
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserName.setText(userName);
        tvPoints = (TextView) findViewById(R.id.tvPoints);
        tvPoints.setText(Integer.toString(points));
        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvQuestion = (TextView) findViewById(R.id.question);
        tvQuestionNumber = (TextView) findViewById(R.id.questionNumber);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        btnFullScreen = (Button) findViewById(R.id.btnFullScreen);

        //Set Button Background Color to default when starting Application
        button1.setBackgroundColor(Color.parseColor(btnDefaultColor));
        button2.setBackgroundColor(Color.parseColor(btnDefaultColor));
        button3.setBackgroundColor(Color.parseColor(btnDefaultColor));
        button4.setBackgroundColor(Color.parseColor(btnDefaultColor));

        //Progressbar on the Screen
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setProgress(count);

        textColorDefaultTimer = tvTimer.getTextColors();

        //create all Arrays for Answers and Questions
        allAnswers = new String[questionAmount][4];
        answer = new String[questionAmount];
        questionArray = new String[questionAmount];

        //create Connection and parse JSON
        MyAsyncTasks myAsyncTasks = new MyAsyncTasks();
        myAsyncTasks.execute();

        //Invisible Button on Fullscreen which detects input after answer selection (after input the next question shows)
        btnFullScreen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                getNewQuestion();
                //disable Fullscreen Button so you can select an answer
                btnFullScreen.setVisibility(View.GONE);

                button1.setBackgroundColor(Color.parseColor(btnDefaultColor));
                button2.setBackgroundColor(Color.parseColor(btnDefaultColor));
                button3.setBackgroundColor(Color.parseColor(btnDefaultColor));
                button4.setBackgroundColor(Color.parseColor(btnDefaultColor));
            }
        });

        //for each button exists a listener and checks if the answer is correct
        //if correct you earn 1 point
        //if correct changes the button to green, else to red and the right answer shows green
        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(button1.getText().toString().equals(answer[count-1])){
                    button1.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    points++;
                }else if(button2.getText().toString().equals(answer[count-1])){
                    button1.setBackgroundColor(Color.parseColor(btnWrongColor));
                    button2.setBackgroundColor(Color.parseColor(btnCorrectColor));
                }else if(button3.getText().toString().equals(answer[count-1])){
                    button1.setBackgroundColor(Color.parseColor(btnWrongColor));
                    button3.setBackgroundColor(Color.parseColor(btnCorrectColor));
                }else if(button4.getText().toString().equals(answer[count-1])){
                    button1.setBackgroundColor(Color.parseColor(btnWrongColor));
                    button4.setBackgroundColor(Color.parseColor(btnCorrectColor));
                }
                checkAnswers();
            }
        });
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(button1.getText().toString().equals(answer[count-1])){
                    button1.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    button2.setBackgroundColor(Color.parseColor(btnWrongColor));
                }else if(button2.getText().toString().equals(answer[count-1])){
                    button2.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    points++;
                }else if(button3.getText().toString().equals(answer[count-1])){
                    button2.setBackgroundColor(Color.parseColor(btnWrongColor));
                    button3.setBackgroundColor(Color.parseColor(btnCorrectColor));

                }else if(button4.getText().toString().equals(answer[count-1])){
                    button2.setBackgroundColor(Color.parseColor(btnWrongColor));
                    button4.setBackgroundColor(Color.parseColor(btnCorrectColor));
                }
                checkAnswers();
            }
        });
        button3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(button1.getText().toString().equals(answer[count-1])){
                    button1.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    button3.setBackgroundColor(Color.parseColor(btnWrongColor));
                }else if(button2.getText().toString().equals(answer[count-1])){
                    button2.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    button3.setBackgroundColor(Color.parseColor(btnWrongColor));
                }else if(button3.getText().toString().equals(answer[count-1])){
                    button3.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    points++;
                }else if(button4.getText().toString().equals(answer[count-1])){
                    button3.setBackgroundColor(Color.parseColor(btnWrongColor));
                    button4.setBackgroundColor(Color.parseColor(btnCorrectColor));
                }
                checkAnswers();
            }
        });
        button4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(button1.getText().toString().equals(answer[count-1])){
                    button1.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    button4.setBackgroundColor(Color.parseColor(btnWrongColor));
                }else if(button2.getText().toString().equals(answer[count-1])){
                    button2.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    button4.setBackgroundColor(Color.parseColor(btnWrongColor));

                }else if(button3.getText().toString().equals(answer[count-1])){
                    button3.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    button4.setBackgroundColor(Color.parseColor(btnWrongColor));
                }else if(button4.getText().toString().equals(answer[count-1])){
                    button4.setBackgroundColor(Color.parseColor(btnCorrectColor));
                    points++;
                }
                checkAnswers();
            }
        });
    }

    public class MyAsyncTasks extends AsyncTask<String, String, String> {

        //create Dialog when results are loading
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("processing results");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        //get the complete JSON answer from the API
        @Override
        protected String doInBackground(String... strings) {
            String result ="";
            try{
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(myUrl);
                    urlConnection = (HttpURLConnection)  url.openConnection();
                    InputStream in = urlConnection.getInputStream();
                    InputStreamReader isr = new InputStreamReader(in);
                    int data = isr.read();
                    while (data != -1){
                        result += (char) data;
                        data = isr.read();
                    }

                    return result;
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if (urlConnection != null){
                        urlConnection.disconnect();
                    }
                }
            }catch(Exception ex){
                ex.printStackTrace();
                return "Exception: " + ex.getMessage();
            }
            return result;
        }

        //set Dialog invisible and parse the String in JSONObjects
        //the Answer and Question arrays are filled from the API
        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();

            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                JSONObject[] jsonObjectsArray = new JSONObject[questionAmount];

                for(int i = 0; i<questionAmount; i++){
                    jsonObjectsArray[i] = jsonArray.getJSONObject(i);
                    questionArray[i] = jsonObjectsArray[i].getString("question");
                    answer[i] = jsonObjectsArray[i].getString("correct_answer");
                    JSONArray jsonAnswersArray = jsonObjectsArray[i].getJSONArray("incorrect_answers");

                    allAnswers[i][0] = answer[i];
                    for(int o = 1; o<4; o++){
                        allAnswers[i][o] = jsonAnswersArray.getString(o-1);
                    }

                }
                //get new Question to display on screen
                getNewQuestion();
            }catch(JSONException jsonException){
                jsonException.printStackTrace();
            }
        }

    }

    private void getNewQuestion() {
        //when you are not at the last question parse the HTML Elements to String (ex. &039;) (without special characters)
        if (count<questionAmount){
            String question = questionArray[count].toString();
            question = Html.fromHtml(question).toString().replaceAll("\n", "").trim();

            //set the text in teh buttons randomly
            setButtonText();

            //set the textview and show the question
            tvQuestion.setText(question);
            tvQuestion.setVisibility(View.VISIBLE);

            //set the Progressbar to the % of the progress
            progressBar.setProgress(count*(100/questionAmount));

            count++;
            tvQuestionNumber.setText("Question "+count);

            //start Countdown
            timeLeftInMillies = TIMER_IN_MILLIES;
            startCountDown();

        }else{
            //set Intent for the next Activity which shows the Results
            Intent intent = new Intent(MainActivity.this, AchievementActivity.class);
            Bundle bundle = new Bundle();

            bundle.putString("name", userName);
            bundle.putInt("points", points);
            bundle.putInt("questionAmount", questionAmount);
            intent.putExtras(bundle);
            startActivity(intent);

            //set points and count to 0
            this.points = 0;
            this.count = 0;
        }
    }

    //concat URL with all selected parameters
    private String setUrl() {
        String newUrl = "";

        //set the question Amount like selected
        newUrl = newUrl.concat("amount="+String.valueOf(questionAmount));

        //set the category as selected in StartActivity (Trivia API-ID starts at 9 and not at 0
        //set difficulty as selected in StartActivity
        if(category.isEmpty()){
        }else{
            categoryId +=9;
            newUrl = newUrl.concat("&category="+categoryId);
        }
        if(difficulty.isEmpty()){

        }else{
            newUrl = newUrl.concat("&difficulty="+difficulty);
        }

        //set the type of all questions to multiple Choice
        newUrl = newUrl.concat("&type=multiple");

        return newUrl;
    }

    //check Answers and cancel the Timer. Set the points and acitvate the Full Screen Button
    private void checkAnswers() {
        countDownTimer.cancel();

        tvPoints.setText(Integer.toString(points));
        btnFullScreen.setVisibility(View.VISIBLE);
    }

    //start the Timer (30s)
    private void startCountDown() {
        countDownTimer = new CountDownTimer(timeLeftInMillies, 1000) {
            @Override
            public void onTick(long l) {
                timeLeftInMillies = l;
                updateTimerText();

            }

            @Override
            public void onFinish() {
                timeLeftInMillies = 0;
                updateTimerText();
                showAnswers();
            }
        }.start();
    }

    //update the Timer Text in the textView
    private void updateTimerText(){
        int minutes = (int) (timeLeftInMillies / 1000) / 60;
        int seconds = (int) (timeLeftInMillies / 1000) % 60;

        String timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        tvTimer.setText(timeFormatted);

        //under 10 seconds, the color changes to red
        if(timeLeftInMillies<10000){
            tvTimer.setTextColor(Color.RED);
        }else{
            tvTimer.setTextColor(textColorDefaultTimer);
        }
    }

    //method to show answers when the timer is at 00:00 and no answer is selected
    private void showAnswers() {
        if(button1.getText().toString().equals(answer[count-1])){
            button1.setBackgroundColor(Color.parseColor(btnCorrectColor));
        }else if(button2.getText().toString().equals(answer[count-1])){
            button2.setBackgroundColor(Color.parseColor(btnCorrectColor));
        }else if(button3.getText().toString().equals(answer[count-1])){
            button3.setBackgroundColor(Color.parseColor(btnCorrectColor));
        }else if(button4.getText().toString().equals(answer[count-1])){
            button4.setBackgroundColor(Color.parseColor(btnCorrectColor));
        }
        checkAnswers();
    }

    //set the answers in the 4 Buttons randomly
    private void setButtonText() {
        Random rand = new Random();
        int value = rand.nextInt(4);

        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);

        //set the buttontext on all 4 Buttons randomly and parse the HTML Elements so it is readable
        button1.setText(Html.fromHtml(allAnswers[count][list.get(value)]).toString().replaceAll("\n", "").trim());
        list.remove(value);
        value = rand.nextInt(3);
        button2.setText(Html.fromHtml(allAnswers[count][list.get(value)]).toString().replaceAll("\n", "").trim());
        list.remove(value);
        value = rand.nextInt(2);
        button3.setText(Html.fromHtml(allAnswers[count][list.get(value)]).toString().replaceAll("\n", "").trim());
        list.remove(value);
        button4.setText(Html.fromHtml(allAnswers[count][list.get(0)]).toString().replaceAll("\n", "").trim());
    }

    //when app is destroyed, the timer needs to stop or there is an error
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(countDownTimer != null){
            countDownTimer.cancel();
        }
    }

    //shows an Dialog for when the user wants to leave the quiz in the Questions
    //Dialog can be dismissed or accepted
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit? Your progress will be lost!");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }

}